var scm = {
	callbacks: {}
};
scm.supported = [ 'git', 'hg' ];
for ( var i in scm.supported )
	scm.callbacks[ scm.supported[ i ] ] = [];
scm.register = function ( scm, command, fn ) {
	if ( scm.supported.indexOf( scm ) == -1 )
		return false;
	scm.callbacks[ scm ][ command ] = fn;
}
scm.parse = function ( scm, command, data ) {
	if ( scm.supported.indexOf( scm ) == -1 )
		return false;
	if ( typeof scm.callbacks[ scm ][ command ] == 'undefined' )
		return false;
	return scm.callbacks[ scm ][ command ]( data );
}
